import { Controller, Delete, Get, Param, Post, Put } from "@nestjs/common";
import { HistService } from "../../service/hist/hist.service";

interface codeSend {
	message: string;
	code: number;
	data: any;
}

@Controller("hist")
export class HistController {
	constructor(private histService: HistService) {}

	@Get()
	async listAll(): Promise<codeSend> {
		try {
			const data = await this.histService.getDataHist();
			return {
				code: 200,
				message: "list-hist",
				data,
			};
		} catch (error) {
			return {
				code: 500,
				message: "list-err",
				data: [error],
			};
		}
	}
	@Post()
	async loadData(): Promise<codeSend> {
		const result = await this.histService.loadDataHist();
		if (result) {
			return {
				code: 200,
				message: "data-load",
				data: [],
			};
		}
		return {
			code: 500,
			message: "error-server-load",
			data: [],
		};
	}
	@Delete("/:id")
	async deleteHist(@Param("id") id: any): Promise<codeSend> {
		try {
			await this.histService.updateHist(id);
			return {
				code: 200,
				message: "delete-hist",
				data: [],
			};
		} catch (error) {
			return {
				code: 500,
				message: "delete-err",
				data: [error],
			};
		}
	}
	@Put("/:id")
	async activeHist(@Param("id") id: any): Promise<codeSend> {
		try {
			await this.histService.updateHist(id, false);
			return {
				code: 200,
				message: "delete-hist",
				data: [],
			};
		} catch (error) {
			return {
				code: 500,
				message: "delete-err",
				data: [error],
			};
		}
	}
}
