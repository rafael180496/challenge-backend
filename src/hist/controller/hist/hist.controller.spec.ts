import { Test, TestingModule } from "@nestjs/testing";
import { HistController } from "./hist.controller";
const listData = [
	{
		_id: "622ba2207c5a124d0fd575d3",
		story_id: 30631943,
		deleted: false,
		created_at: "2022-03-11T18:17:59.000Z",
		title: null,
		url: null,
		story_title: "Toward a better list iterator for the Linux kernel",
		comment_text:
			"If your argument is &quot;linked list memory management is rocket science so nobody should ever do it&quot; then please stop. You&#x27;re only digging yourself deeper.<p>No serious linked list implementation in Rust uses Rc&lt;Refcell&lt;_&gt;&gt;. They all use either unsafe or a vector of nodes because linked lists are among the most studied data structures in all of computer science and are extremely well-understood by skilled practitioners.<p>The people who wrote C and Pascal for decades didn&#x27;t &quot;straight up ignore the safety aspects.&quot; This is pure projection and only makes the Rust community (which I consider myself a part of) look bad.",
		author: "platinumrad",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575d6",
		story_id: 30642281,
		deleted: false,
		created_at: "2022-03-11T17:58:18.000Z",
		title: null,
		url: null,
		story_title: "Convert curl commands to code in several languages",
		comment_text:
			"Re-using curl&#x27;s C code and compiling to WASM is the dream.<p>libcurl does stuff like open files for reading, so you would have to at least modify the code to just pass the file name, you also have to modify the struct.<p>libcurl parses an array of strings, but curlconverter gets a Bash AST (or a list of strings if you use it from the command line). When there&#x27;s a bash variable in the command, we want to generate code that gets that environment variable at runtime, e.g. `os.environ[&#x27;MY_VARIABLE&#x27;]` in Python so the struct needs to store pointers to AST nodes instead of strings&#x2F;booleans&#x2F;ints. Though to be fair curlconverter doesn&#x27;t work this way yet, we convert AST nodes to strings&#x2F;booleans&#x2F;ints after command parsing and store that, then when we&#x27;re generating the code if we see a $ in a string we assume that was a variable in the input AST.",
		author: "verhovsky",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575d9",
		story_id: 30634872,
		deleted: false,
		created_at: "2022-03-11T16:32:59.000Z",
		title: null,
		url: null,
		story_title: "Earn-IT threatens encryption and therefore user freedom",
		comment_text:
			'Exit nodes are specifically set up and run, you do not become one by default. Using the Tor browser doesn&#x27;t even make you a relay node: <a href="https:&#x2F;&#x2F;support.torproject.org&#x2F;tbb&#x2F;tbb-33&#x2F;" rel="nofollow">https:&#x2F;&#x2F;support.torproject.org&#x2F;tbb&#x2F;tbb-33&#x2F;</a>',
		author: "miloignis",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575dc",
		story_id: 30634245,
		deleted: false,
		created_at: "2022-03-11T16:20:49.000Z",
		title: null,
		url: null,
		story_title: "Just Say No to Central Bank Digital Currencies",
		comment_text:
			"It depends on if we do end up with only one party, or if banks end up running nodes too. I believe Visas blockchain projects require institutions to run nodes. If you are using USD-CBDC as a reserve currency for your country you&#x27;d sure as hell want to make sure that the currencies aren&#x27;t being changed from under your feet, as would banks sending money between each other.",
		author: "exdsq",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575df",
		story_id: 30624154,
		deleted: false,
		created_at: "2022-03-11T15:30:10.000Z",
		title: null,
		url: null,
		story_title:
			"Why offer an Onion Address rather than just encourage browsing-over-Tor?",
		comment_text:
			"&gt;TLS&#x2F;HSTS is still subject to CA attacks, e.g. diginotar.<p>Largely solved by Certificate Transparency. If you compromise a CA, you can issue certificates. However, you can&#x27;t issue new certificates without broadcasting that fact to the whole world as browsers will not accept certificates without SCTs.<p>&gt;Reducing load on exit nodes is a technical benefit that&#x27;s in that blog post.<p>This hasn&#x27;t been a real benefit for years. Exit nodes are running at something like 10% capacity.<p>&gt;Another benefit to using Tor onion services for large sites is that the Tor circuit ID can be used as an additional key in an IP rate limit cache. This helps block Tor bots (on the basis that establishing a Tor circuit is expensive).<p>This is just another problem with hidden services. Opening circuits costs malicious clients far less cpu time than it costs the server.",
		author: "Asan1",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575e3",
		story_id: null,
		deleted: false,
		created_at: "2022-03-11T12:47:05.000Z",
		title: "Read a Node.js stream using async/await",
		url: "https://www.nodejsdesignpatterns.com/blog/node-js-stream-consumer/",
		story_title: null,
		comment_text: null,
		author: "loige",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575e8",
		story_id: 30635989,
		deleted: false,
		created_at: "2022-03-11T08:39:30.000Z",
		title: null,
		url: null,
		story_title: "Investigating Influencer VPN Ads on YouTube [pdf]",
		comment_text:
			"I feel like these have gotten better over time.  The pitch used to be that your ISP was spying on you, now the pitch is that you can pretend to be in a different country.  That&#x27;s closer to the truth.  (But of course, everyone has the list of VPN exit nodes and just blocks them.  I wonder if any of these VPN services are actually peer to peer, so that your IP is an exit address for people that want to pretend to be in your country.  The legal liability sounds staggering, but I guess that&#x27;s why you set up your company in some country that doesn&#x27;t care, accept crypto for payments, and hope for the best.)",
		author: "jrockway",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575eb",
		story_id: 30627489,
		deleted: false,
		created_at: "2022-03-11T08:17:36.000Z",
		title: null,
		url: null,
		story_title: "Phases of Netflix’s real-time data infrastructure",
		comment_text:
			"&gt;One reason Kubernetes is so fast<p>I spit out my Pepsi at this. Some of us have actually used kubernetes with 5,000+ nodes. It&#x27;s orders of magnitude slower than Mesos",
		author: "beebmam",
		__v: 0,
	},
	{
		_id: "622ba2207c5a124d0fd575f6",
		story_id: 30626924,
		deleted: false,
		created_at: "2022-03-10T21:57:18.000Z",
		title: null,
		url: null,
		story_title: "Anatomy of an AI System (2018)",
		comment_text:
			"The real point of it for me is the framing. It&#x27;s the idea that any object, tangible or intangible, and be analyzed in this way because everything has multiple dimensions of relations linking it to every other thing eventually. It&#x27;s the equivalent of being able to pick up the graph of human relations by a single point and letting all other relations branch from that point like a tree. There&#x27;s obviously root nodes that are more interesting than others just as there are more spanning trees more interesting than all the others.",
		author: "kelseyfrog",
		__v: 0,
	},
];
describe("HistController", () => {
	let controller: HistController;

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			controllers: [HistController],
		}).compile();

		controller = module.get<HistController>(HistController);
	});

	it("should be defined", () => {
		expect(controller).toBeDefined();
	});

	it("should be loadData", async () => {
		const data = await controller.loadData();
		expect(data.data).toBe(true);
	});

	it("should be list", async () => {
		const resp = await controller.listAll();
		expect(resp.data).toBe(listData);
	});

	it("should be delete", async () => {
		const data = await controller.deleteHist("30627489");
		expect(data.code).toBe(200);
	});
	it("should be active", async () => {
		const data = await controller.activeHist("30627489");
		expect(data.code).toBe(200);
	});
});
