import { Injectable, Logger } from "@nestjs/common";
const axios = require("axios").default;
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";
import { Hist } from "../model";
import { Cron, CronExpression } from "@nestjs/schedule";
@Injectable()
export class HistService {
	constructor(@InjectModel(Hist.name) private histModel: Model<Hist>) {}
	private readonly logger = new Logger(HistService.name);
	async loadDataHist(): Promise<boolean> {
		try {
			const response = await axios.get(
				"https://hn.algolia.com/api/v1/search_by_date?query=nodejs",
			);
			if (response.status === 200) {
				const { hits } = response.data;
				for (const hit of hits) {
					const exist = await this.histModel.find({
						story_id: { $eq: hit.story_id },
					});
					if (exist.length == 0) {
						const created = new this.histModel(hit);
						await created.save();
					}
				}

				return true;
			} else {
				return false;
			}
		} catch (error) {
			console.error(error);
			return false;
		}
	}
	async getDataHist() {
		return await this.histModel.find({ deleted: { $eq: false } }).exec();
	}
	@Cron("*/45 * * * *", {
		name: "dataload",
	})
	async loadDataTask() {
		this.logger.debug("Star Cron LoadData:");
		const valid = await this.loadDataHist();
		this.logger.debug("End Cron LoadData:", valid);
	}
	async updateHist(id: string, deleted: boolean = true) {
		return await this.histModel.updateOne(
			{ story_id: { $eq: id } },
			{ $set: { deleted: deleted } },
		);
	}
}
