import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema()
export class Hist extends Document {
	@Prop()
	author: String;
	@Prop()
	comment_text: String;
	@Prop()
	story_title: String;
	@Prop()
	url: String;
	@Prop()
	title: String;
	@Prop()
	created_at: Date;
	@Prop({ default: false })
	deleted: Boolean;
	@Prop({ index: true })
	story_id: Number;
}

export const HistSchema = SchemaFactory.createForClass(Hist);
