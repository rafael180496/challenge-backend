import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { MongooseModule } from "@nestjs/mongoose";
import { Hist, HistSchema } from "./hist/service/model";
import { HistService } from "./hist/service/hist/hist.service";
import { HistController } from "./hist/controller/hist/hist.controller";
import { ScheduleModule } from "@nestjs/schedule";
const url = process.env.MONGO_DSN || "localhost";
@Module({
	imports: [
		ScheduleModule.forRoot(),
		MongooseModule.forRoot(`mongodb://${url}:27017/challenge`),
		MongooseModule.forFeatureAsync([
			{
				name: Hist.name,
				useFactory: () => {
					const schema = HistSchema;
					return schema;
				},
			},
		]),
	],
	controllers: [AppController, HistController],
	providers: [AppService, HistService],
})
export class AppModule {}
